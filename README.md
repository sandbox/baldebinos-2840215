CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting

INTRODUCTION
------------

The Simple Video Subtitles module allows you to display video files hosted in your server.
It creates a Field Formatter, available for fields of type File.
Does NOT use any java scripts or CSS and depends on the capability of the browser to render HTML5 Video.
Supports:

 * mp4, ogg and webm as video formats.
 * vtt as subtitles format.
 * gif or jpg as images for the poster.

Basic features:

 * Setting Width and Height of the player.
 * Additional custom CSS Classes.
 * Setting Autoplay.
 * Setting Muted.
 * Usage of subtitles (.vtt).
   * If using subtitles it is recommended to "Enable Description field" option.
   * The content of this field will be used for displaying the language of each subtitle.
 * Usage of poster (.jpg or .gif).
   * Although most browsers will show the first frame of the video before starting playing, some mobile browsers will not.
   * To avoid the player to initially render blank in some platforms, uploading a gif or jpg image for use as poster is recommended.
   * If you upload several images only the top one will be used for the poster. Rearrange the order of the files to select the image to use.

REQUIREMENTS
------------

 * core/jquery
 
INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module.

CONFIGURATION
-------------

 * Go to Structure -> Content types, and choose Add content type (or use an existing one).
 * For that Content type choose Manage fields.
 * Choose Add field.
 * In Select a field type, choose File.
 * Enter an appropriate Label. Example: Video.
 * As Allowed number of values, type the required number or choose Unlimited.
 * As Allowed file extensions, type the extensions you plan to use. Example: mp4, vtt, jpg.
   * If you enable and upload files with extensions other then mp4, ogg, webm, vtt, gif or jpg, they will be ignored.
 * Check the "Enable Description field" option. The Description field will be used to identify the language of the subtitles.
   * When uploading vtt files, use the Description to identify the language of the subtitle. Example: Português.
 * On the Manage display tab, under FORMAT, choose Simple Video Subtitles for the newly created field.
 * Optionally, you can set the Width, Height and specific CSS Classes for the player.
   * Width and Height are required and must specify the value and units or use auto.
   * CSS Classes is optional. Check your theme documentation for the appropriate classes to use.
 * You can also set the behavior of the player checking the Autoplay and Mute options.
 * Now, add new content to your site and have fun.

TROUBLESHOOTING
---------------

 * Video files may be very large. You may need to change the php.ini file to allow uploading such large files.
 * Set post_max_size, upload_max_filesize and max_file_uploads accordingly to your needs.