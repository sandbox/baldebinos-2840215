<?php

/**
 * @file
 * Contains \Drupal\simple_video_subtitles\Plugin\Field\FieldFormatter\SimpleVideoSubtitlesFormatter.
 */

namespace Drupal\simple_video_subtitles\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'simple_video_subtitles' formatter.
 *
 * @FieldFormatter(
 *   id = "simple_video_subtitles_formater",
 *   label = @Translation("Simple Video Subtitles"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */

class SimpleVideoSubtitlesFormatter extends SimpleVideoSubtitlesFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'width' => '100%',
      'height' => 'auto',
      'class' => '',
      'autoplay' => FALSE,
      'muted' => FALSE,
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['width'] = [
      '#title' => t('Width'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('width'),
      '#required' => TRUE,
      '#description' => t('The width of the player. Value and units must be specified. The default value is 100%.'),
    ];
    $element['height'] = [
      '#title' => t('Height'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('height'),
      '#required' => TRUE,
      '#description' => t('The height of the player. Value and units must be specified. The default value is auto.'),
    ];
    $element['class'] = [
      '#title' => t('CSS Classes'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('class'),
      '#required' => FALSE,
      '#description' => t('The CSS classes for the player. Check your theme documentation for the appropriate class.'),
    ];
    $element['autoplay'] = [
      '#title' => t('Autoplay'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('autoplay'),
    ];
    $element['muted'] = [
      '#title' => t('Muted'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('muted'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $summary[] = t('Width: @width /  Height: @height', [
      '@width' => $this->getSetting('width'),
      '@height' => $this->getSetting('height'),
      '@class' => $this->getSetting('class'),
      '@autoplay' => $this->getSetting('autoplay') ? t(', autoplay') : '' ,
      '@muted' => $this->getSetting('muted') ? t(', muted') : '',
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();
    $files = $this->getEntitiesToView($items, $langcode);

    /**
     * Early opt-out if the field is empty.
     */
    if (empty($files)) {
      return $elements;
    }

    $poster = array();
    $videos = array();
    $subs = array();

    /**
     * Iterate $files and set $elements.
     */
    foreach ($files as $delta => $file) {

      $file_uri = $file->getFileUri();
      $file_url = Url::fromUri(file_create_url($file_uri));
      $file_ext = pathinfo($file_uri, PATHINFO_EXTENSION);

      /**
       * The description is stored on the file field, not on the file entity.
       * The values from the field can be found in $items[$delta].
       * Using $file->_referringItem to access $items[$delta] and extract
       * the description.
       */
      $item = $file->_referringItem;
      $file_description = $item->description;

      /**
       * Splits Poster, Videos and Subtitles.
       */

      if($file_ext == 'gif' || $file_ext == 'jpg') {
        /**
         * Only the first image is loaded as poster.
         */
        if (empty($poster)) {
          $poster = array('url' => $file_url);
        }
      }
      if($file_ext == 'mp4' || $file_ext == 'ogg' || $file_ext == 'webm') {
        array_push($videos, array('src' => $file_url, 'type' => $file_ext, 'description' => $file_description));
      }

      if($file_ext == 'vtt') {
        array_push($subs, array('src' => $file_url, 'label' => $file_description));
      }

      /**
       * Set $elements.
       */
      $elements[$delta] = array(
        '#theme' => 'SimpleVideoSubtitles',
        '#settings' => $this->getSettings(),
        '#poster' => $poster,
        '#videos' => $videos,
        '#subs' => $subs,
      );
    }

    /**
     * Delete all arrays in $elements but the last one
     * that contains all necessary information.
     * Without deletion duplications will occur.
     * It may not be pretty but is
     * simple, readable, short and it works.
     */
    $elements_count =  count($elements);
    for($i = 0; $i <= $elements_count - 2; $i++) {
      unset($elements[$i]);
    }

    return $elements;
  }
}
