<?php

/**
 * @file
 * Contains \Drupal\simple_video_subtitles\Plugin\Field\FieldFormatter\SimpleVideoSubtitlesFormatterBase.
 */

namespace Drupal\simple_video_subtitles\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;

/**
 * Base class for SimpleVideoSubtitlesFormatter file formatters.
 */
abstract class SimpleVideoSubtitlesFormatterBase extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  protected function getEntitiesToView(EntityReferenceFieldItemListInterface $items, $langcode) {
    return parent::getEntitiesToView($items, $langcode);
  }
}